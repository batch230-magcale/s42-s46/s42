const bcrypt = require("bcrypt");
const auth = require("../auth");

const User = require("../models/users");
const Product = require("../models/products");

// ------------------ s42 --------------------

module.exports.registerUser = async(request) => {
	let newUser = new User({
		firstName: request.firstName,
		lastName: request.lastName,
		email: request.email,
		mobileNumber: request.mobileNumber,
		// userName: request.userName,
		password: bcrypt.hashSync(request.password, 15)
	});

		return User.findOne({$or: [{mobileNumber: request.mobileNumber}, {email: request.email}]})
	    .then(result => {
	        if(result != null && result.email == request.email){
	            return ("Email is already used.");
	        }
	        else if(result != null && result.mobileNumber == request.mobileNumber){
	            return ("Mobile number is already used.");
	        } 
			else{
				 // return false;
				return newUser.save().then((user, error) => {
					if(error){
						return error;
					}
					else{
						return request;
					}
				})
				.catch(error =>{
					console.log(error);
					response.send(false);
				})
				
			}

	})
}


module.exports.loginUser = (request) => {
	return User.findOne({email: request.email}).then(result => {
		if(result == null){
			return ("Email is incorrect!");
		}
		else{
			const userPassword = bcrypt.compareSync(request.password, result.password);

			if(userPassword){
				return {access: auth.createAccessToken(result)};
			}
			else{
				return ("Password is incorrect!");
			}
		}
	})
}


// ------------------ s45 --------------------
// Non-admin User checkout (Create Order)

module.exports.userCheckout = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	Product.findById(request.body.productId)
		.then(async result => {
			if(userData.isAdmin == true){
				response.send ("Please login as guest user.");
			}

			else{
				let productName = await result.name;
				let price = await result.price;
				let totalAmount = await request.body.quantity * price;
				let stocks = await result.stocks;
				let quantity = await result.quantity;
				let isActive = await result.isActive;
				let reqQuantity = await request.body.quantity;

				let newData = {		
					userId: userData.id,
					userName: userData.userName,
					userEmail: userData.email,
					stocks:  stocks,
					isActive: isActive,
					quantity: request.body.quantity,
					productId: request.body.productId,
					totalAmount: totalAmount,
					products: [{		
						productId: request.body.productId,
						productName: productName,
						quantity: request.body.quantity
					}]
				};

				console.log(newData);

				let isProductUpdated = await Product.findById(newData.productId).then(products =>{
						products.orders.push({
							userId: newData.userId,
							userName: newData.userName,
							userEmail: newData.userEmail,
							quantity: newData.quantity
						})
						if(products.stocks <= 0){			
							return ("No stocks available.");
						}
						else if(request.body.quantity > products.stocks){
							return("Not enough stocks.");
						}
						else{
							products.stocks = products.stocks - newData.quantity;
							if(products.stocks == 0) {
							    products.isActive = false;
							}


							return products.save()
							.then(result =>{
								console.log(result);
								return true;
							})
							.catch(error => {
								console.log(error);
								return false;
							});
						}
					})
					console.log(isProductUpdated);

					let isUserUpdated = await User.findById(newData.userId)
					.then(users => {
						if(newData.stocks == 0){
							return ("No stocks available.");
						}
						else if(reqQuantity > stocks && stocks > 0){
							return ("Not enough stocks.");
						}
						else{
							users.orders.push({
								productName: newData.productName,
								quantity: newData.quantity,
								totalAmount: newData.totalAmount,
								products: newData.products
							});
						
							
							return users.save()
							.then(result => {
								console.log(result);
								return true;
							})
							.catch(error => {
								console.log(error);
								return false;
							})
						}
					})

					console.log(isUserUpdated);

					

							/*	
								(isUserUpdated == true && isProductUpdated == true)? response.send("User and Products are updated"): response.send("Not updated");
							*/
					
					if(isUserUpdated && isProductUpdated == true){
						response.send(`Order Successful.`);
					}
					else if(isUserUpdated == false){
						response.send(error);
					}
					else if(isProductUpdated && isUserUpdated == "No stocks available."){
						response.send("No stocks available as of the moment.")
					}
					else if(isProductUpdated && isUserUpdated == "Not enough stocks."){
						response.send(`Not enough stocks, there are only ${stocks} stock/s of ${productName} left.`);
					}
					else if(isProductUpdated == false){
						response.send("Product was not updated.");
					}
					else if(isProductUpdated == "Product is Inactive."){
						response.send("Product is Inactive.");
					}
					else{
						response.send("User and Products were not updated");
					}
				
			}
		})
}



// Retrieve User Details
module.exports.getUserDetails = (userId) => {
	return User.findById(userId).then(result => {
		result.password = "**********";
		return result;
	})
}





//  ---------STRETCH GOAL ------------- (Unfinished)
/*
	- Set user as admin (Admin only)
	- Retrieve authenticated user’s orders
	- Retrieve all orders (Admin only)
	- Add to Cart (Highly suggested)
		- Added Products
		- Change product quantities
		- Remove products from cart
		- Subtotal for each item
		- Total price for all items

*/
// Set user as admin (Admin only)
module.exports.updateUserRole = (userId, archived) => {
	if(archived.isAdmin == true){
		return User.findByIdAndUpdate(userId,
			{
				isAdmin: archived.user.isAdmin
			}
		).then((result, error)=>{
			if(error){
				return false;
			}
			result.password = "********";
			return result;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}



// Retrieve authenticated user’s orders
module.exports.getUserOrders = (userId, token) => {
	if(token.isAdmin == true){
		return User.findById(userId).then(result => {
			console.log(token);
			return result;
		}).then((result, error)=>{
			if(error){
				return false;
			}
			return result.orders;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}


// Retrieve all orders (Admin only)
module.exports.getUsersOrders = (usersOrders) => {
	if(usersOrders.isAdmin == true){
		return User.find().then(result => {
			const buyer = result.flatMap(user => user.orders);
			return buyer;
		})
	}
	else{
		let message = Promise.resolve('No admin rights to access this feature.');
		return message.then((value) => {return value});
	}
}


// Add to Cart
module.exports.addToCart = (request, response) => {

	const userData = auth.decode(request.headers.authorization);
	Product.findById(request.body.productId)
		.then(async result => {
			if(userData.isAdmin == true){
				response.send ("Please login as guest user.");
			}

			else{
				let productName = await result.name;
				let price = await result.price;
				let totalAmount = await request.body.quantity * price;
				let stocks = await result.stocks;
				let quantity = await result.quantity;
				let isActive = await result.isActive;
				let reqQuantity = await request.body.quantity;

				let newData = {		
					userId: userData.id,
					userName: userData.userName,
					userEmail: userData.email,
					stocks:  stocks,
					isActive: isActive,
					quantity: request.body.quantity,
					productId: request.body.productId,
					totalAmount: totalAmount,
					products: [{		
						productId: request.body.productId,
						productName: productName,
						quantity: request.body.quantity
					}]
				};

				console.log(newData);

				let isProductUpdated = await Product.findById(newData.productId).then(products =>{
						products.orders.push({
							userId: newData.userId,
							userName: newData.userName,
							userEmail: newData.userEmail,
							quantity: newData.quantity
						})
						if(products.stocks <= 0){			
							return ("No stocks available.");
						}
						else if(request.body.quantity > products.stocks){
							return("Not enough stocks.");
						}
						else{
							products.stocks = products.stocks - newData.quantity;
							if(products.stocks == 0) {
							    products.isActive = false;
							}
							else{
								console.log(products);
								return true
							}
						}
					})
					console.log(isProductUpdated);

					let isUserUpdated = await User.findById(newData.userId)
					.then(users => {
						if(newData.stocks == 0){
							return ("No stocks available.");
						}
						else if(reqQuantity > stocks && stocks > 0){
							return ("Not enough stocks.");
						}
						else{
							users.addToCart.push({
								productName: newData.productName,
								quantity: newData.quantity,
								totalAmount: newData.totalAmount,
								products: newData.products
							});
						
							
							return users.save()
							.then(result => {
								console.log(result);
								return true;
							})
							.catch(error => {
								console.log(error);
								return false;
							})
						}
					})

					console.log(isUserUpdated);

					

							/*	
								(isUserUpdated == true && isProductUpdated == true)? response.send("User and Products are updated"): response.send("Not updated");
							*/
					
					if(isUserUpdated && isProductUpdated == true){
						response.send(`Product Added to Cart.`);
					}
					else if(isUserUpdated == false){
						response.send(error);
					}
					else if(isProductUpdated && isUserUpdated == "No stocks available."){
						response.send("No stocks available as of the moment.")
					}
					else if(isProductUpdated && isUserUpdated == "Not enough stocks."){
						response.send(`Not enough stocks, there are only ${stocks} stock/s of ${productName} left.`);
					}
					else if(isProductUpdated == false){
						response.send("Product was not updated.");
					}
					else if(isProductUpdated == "Product is Inactive."){
						response.send("Product is Inactive.");
					}
					else{
						response.send("User and Products were not updated");
					}
				
			}
		})
}















// ---------- for own-use functions ---------------

// GET ALL USERS INFO
module.exports.getAllUsers = () => {
	return User.find().then(result => {
		return result;
	})
}


// DELETE USER BY ID
module.exports.deleteUser = (userId) => {
	return User.findByIdAndDelete(userId).then(result => {
		return result;
	})
}

// DELETE ALL USERS
module.exports.deleteAllUsers = () => {
	return User.deleteMany().then(result => {
		return result;
	})
}


