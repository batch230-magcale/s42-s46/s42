const mongoose = require("mongoose");
const express = require("express");
const router = express.Router();
const auth = require("../auth");

const userController = require("../controllers/userControllers");

// ------------------ s42 --------------------
// REGISTER USER
router.post("/register", (request, response) => {
	userController.registerUser(request.body).then(resultFromController => response.send(resultFromController))
})


// LOGIN
router.post("/login", (request, response) => {
	userController.loginUser(request.body).then(resultFromController => response.send(resultFromController));
})




// ------------------ s45 --------------------
// Non-admin User checkout (Create Order)

router.post("/checkout", auth.verify, userController.userCheckout);




// Retrieve User Details
router.get("/:userId/userDetails", (request, response) => {
	userController.getUserDetails(request.params.userId).then(resultFromController => response.send(resultFromController));
})


//  ---------STRETCH GOAL ------------- (Unfinished)
/*
	- Set user as admin (Admin only)
	- Retrieve authenticated user’s orders
	- Retrieve all orders (Admin only)
	- Add to Cart (Highly suggested)
		- Added Products
		- Change product quantities
		- Remove products from cart
		- Subtotal for each item
		- Total price for all items

*/
// Set user as admin (Admin only)
router.patch("/:userId/roleUpdate", auth.verify, (request,response) => 
	{
		const archived = {
			user: request.body,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		userController.updateUserRole(request.params.userId, archived).then(resultFromController => {
			response.send(resultFromController)
		})
})


// Retrieve authenticated user’s orders
router.get("/:userId/getUserOrders", auth.verify, (request,response) => 
	{
		const token = {
			// user: request.params.userId,
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}

		userController.getUserOrders(request.params.userId, token).then(resultFromController => {
			response.send(resultFromController)
		})
})


// Retrieve all orders (Admin only)
router.get("/usersWithOrders", auth.verify, (request,response) => 
	{
		const usersOrders = {
			isAdmin: auth.decode(request.headers.authorization).isAdmin
		}
		userController.getUsersOrders(usersOrders).then(resultFromController => {
			response.send(resultFromController)
		})
})


// Add to Cart
router.post("/addToCart", auth.verify, userController.addToCart);





// ---------- for own-use functions ---------------

// GET ALL USERS INFO
router.get("/getAllUsers", (request, response) => {
	userController.getAllUsers().then(resultFromController => response.send(resultFromController));
})



// DELETE USER BY ID
router.delete("/:userId/delete", (request, response) => {
	userController.deleteUser(request.params.userId).then(resultFromController => response.send(resultFromController));
})


// DELETE ALL USERS
router.delete("/deleteAllUsers", (request, response) => {
	userController.deleteAllUsers().then(resultFromController => response.send(resultFromController));
})









module.exports = router;